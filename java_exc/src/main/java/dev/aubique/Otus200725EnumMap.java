package dev.aubique;

import java.util.EnumMap;
import java.util.Map;

public class Otus200725EnumMap {

    public static void main(String[] args) {
        Map<Version, String> server = new EnumMap<>(Version.class);
        server.put(Version.ONE, "First server version");
        server.put(Version.TWO, "Second server version");

        Map<Version, String> client = new EnumMap<Version, String>(server);
        client.clear();

        client.put(Version.THREE, "Third client version");
        client.putAll(Map.copyOf(server));

        System.out.println(client);
        System.out.println(server);
    }

    enum Version {
        ONE(1), TWO(2), THREE(3);

        private int version;

        Version(int version) {
            this.version = version;
        }

        public int getVersion() {
            return version;
        }
    }
}
