package dev.aubique;

interface MessageBuilder200524 {
    String buildMessage(String templateName, String text, String signature);
}

interface TemplateProvider200524 {
    String getMessageTemplate(String templateName);
}

public class Otus200524MessageBuilderImpl implements MessageBuilder200524 {

    private final TemplateProvider200524 templateProvider;

    public Otus200524MessageBuilderImpl(TemplateProvider200524 templateProvider) {
        this.templateProvider = templateProvider;
    }

    @Override
    public String buildMessage(String templateName, String text, String signature) {
        String messageTemplate = templateProvider.getMessageTemplate(templateName);
        if (messageTemplate == null || messageTemplate.isEmpty()) {
            throw new TemplateException200524();
        }
        return String.format(messageTemplate, text, signature);
    }
}

class TemplateException200524 extends RuntimeException {
}
