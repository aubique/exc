package dev.aubique;

/*
java -Xdebug Xrunjdwp:transport=dt_socket,address=5005,server=y,suspend=n -jar Otus200708RemoteDebugger-0.1.jar
 */
public class Otus200708RemoteDebugger {

    private int value;

    public static void main(String[] args) throws InterruptedException {
        new Otus200708RemoteDebugger().loop();
    }

    private void loop() throws InterruptedException {
        while (!Thread.currentThread().isInterrupted()) {
            value += 10;
            incVal();
            System.out.println(value);
            Thread.sleep(2_000);
        }
    }

    private void incVal() {
        value += 1231;
    }
}
