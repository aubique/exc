package dev.aubique;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Jar200608ContinueStatement {

    public static void main(String[] args) {
        int count = 0;

        List<Integer> numbers = IntStream.range(1, 100).boxed().collect(Collectors.toList());
        for (int num : numbers) {
            try {
                System.out.println();
                System.out.println(veryifyEven(num));
            } catch (Exception ex) {
                System.out.println(num + "\t\t = NOT EVEN number");
                continue;
            } finally {
                System.out.println("FINALLY");
            }
            System.out.println("COUNT++");
            count++;
        }

        System.out.println(String.format("=====\ncount = %d\n=====", count));
    }

    private static String veryifyEven(int number) throws Exception {
        if (number % 2 != 0)
            throw new Exception("That number is not even");
        return String.format("%d\t\t = EVEN number", number);
    }
}
