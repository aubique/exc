package dev.aubique.cw;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Cw200614Carboat {

    public static String howmuchImproved(int m, int n) {
        List<String> solutions = IntStream.rangeClosed(Math.min(m, n), Math.max(m, n))
                .boxed()
                .filter(f -> f % 9 == 1 && f % 7 == 2)
                .map(f -> "[M: " + f + " B: " + f / 7 + " C: " + f / 9 + "]")
                .collect(Collectors.toList());
        return "[" + String.join((""), solutions) + "]";
    }

    /**
     * CodeWars: How Much?
     * https://www.codewars.com/kata/55b4d87a3766d9873a0000d4
     */
    public static String howmuch(int m, int n) {
        final StringBuilder fortuneResult = new StringBuilder("[");
        int min = Math.min(m, n);
        int max = Math.max(m, n);

        for (int i = min; i <= max; i++) {
            String costText = null;

            if ((costText = calcCost(i)) != null)
                fortuneResult.append(costText);
        }
        fortuneResult.append("]");

        return fortuneResult.toString();
    }

    /**
     * x - 7y = 2 (boats)
     * x - 9z = 1 (cars)
     */
    public static String calcCost(int number) {
        final float fortune = (float) number;
        final float boats = (fortune - 2f) / 7f;
        final float cars = (fortune - 1f) / 9f;

        if (boats != Math.round(boats))
            return null;
        if (cars != Math.round(cars))
            return null;

        return String.format(
                "[M: %d B: %d C: %d]", (int) fortune, (int) boats, (int) cars);
    }

    public static int getCountImproved(String str) {
        int vowelsCount = 0;
        for (char c : str.toCharArray()) {
            vowelsCount += (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') ? 1 : 0;
        }
        return vowelsCount;
    }

    /**
     * CodeWars: Vowel Count
     * https://www.codewars.com/kata/54ff3102c1bad923760001f3
     */
    public static int getCount(String str) {
        int vowelsCount = 0;
        final char[] chars = str.toCharArray();
        for (char c : chars) {
            if (isVowel(c))
                vowelsCount++;
        }

        return vowelsCount;
    }

    public static boolean isVowel(char c) {
        return Character.toLowerCase(c) == 'a'
                || Character.toLowerCase(c) == 'e'
                || Character.toLowerCase(c) == 'i'
                || Character.toLowerCase(c) == 'o'
                || Character.toLowerCase(c) == 'u';
    }
}
