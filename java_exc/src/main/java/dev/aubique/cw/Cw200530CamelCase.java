package dev.aubique.cw;

public class Cw200530CamelCase {

    public static String toCamelCase(String input) {
        StringBuilder finalText = new StringBuilder();
        String[] words = input.split(" ");

        finalText.append(words[0]);
        for (int i = 1; i < words.length; i++) {
            finalText.append(camelCaseWord(words[i]));
        }
        return finalText.toString();
    }

    private static String camelCaseWord(String word) {
        char[] chars = word.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);

        return String.valueOf(chars);
    }

    /**
     * CodeWars: Break camelCase
     * https://www.codewars.com/kata/5208f99aee097e6552000148/java
     */
    public static String toNormalText(String input) {
        StringBuilder finalText = new StringBuilder();
        final char[] chars = input.toCharArray();

        for (char c : chars) {
            if (Character.isUpperCase(c))
                finalText.append(' ');
            finalText.append(c);
        }
        return finalText.toString();
    }
}
