package dev.aubique.cw;

public class Cw200524DigitCount {

    /**
     * CodeWars: Count the Digit
     * https://www.codewars.com/kata/566fc12495810954b1000030/java
     */
    public static int nbDig(int n, int d) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += countDigInNum((int) Math.pow(i, 2), d);
        }
        return sum;
    }

    private static int countDigInNum(int num, int dig) {
        int count = 0;
        do {
            if ((num % 10) == dig)
                count++;
        } while ((num = num / 10) > 0);
        return count;
    }
}
