package dev.aubique;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * Test class {@link Otus200524MessageBuilderImpl} with JUnit5, Mockito and AssertJ
 * Sources: {@code https://github.com/petrelevich/otus_java_2020_03/blob/master/L05-qa/src/test/java/ru/otus/testing/example/services/CalculatorServiceImplTest.java}
 */
@DisplayName("Класс MessageBuilderImpl")
class Otus200524MessageBuilderImplTest {

    public static final String TEMPLATE_NAME = "AnyTemplate";
    public static final String TEMPLATE_TEXT = "Hi!\n %s \n With best regards, %s";
    public static final String MESSAGE_TEXT = "How you doing?";
    public static final String SIGN = "Alex";

    private TemplateProvider200524 provider;
    private Otus200524MessageBuilderImpl msgBuilder;

    @BeforeEach
    void setUp() {
        provider = mock(TemplateProvider200524.class);
        msgBuilder = new Otus200524MessageBuilderImpl(provider);
    }

    /**
     * Сделать тест, который проверяет, что если шаблон найден, {@code buildMessage} корректно форм. сообщение
     */
    @DisplayName("должен создать верное сообщение для заданного шаблона, текста и подписи")
    @Test
    public void shouldBuildCorrectMessageForGivenTemplateByTextAndSign() {
//        when(provider.getMessageTemplate(TEMPLATE_NAME)).thenReturn(TEMPLATE_TEXT);
        given(provider.getMessageTemplate(TEMPLATE_NAME)).willReturn(TEMPLATE_TEXT);
        String actualMessage = msgBuilder.buildMessage(TEMPLATE_NAME, MESSAGE_TEXT, SIGN);
//        Assertions.assertEquals(String.format(TEMPLATE_TEXT, MESSAGE_TEXT, SIGN), actualMessage);
        assertThat(String.format(TEMPLATE_TEXT, MESSAGE_TEXT, SIGN)).isEqualTo(actualMessage);
    }

    /**
     * Сделать тест, который проверяет, что внутри {@code buildMessage} один раз и с нужным
     * параметром, вызывается метод {@code getMessageTemplate}, класса {@link TemplateProvider200524}
     */
    @DisplayName("должен единожды вызвать нужный метод зависимости при создании сообщения")
    @Test
    public void shouldFireOnceExpectedDependencyMethodWhileBuildMessage() {
//        when(provider.getMessageTemplate(TEMPLATE_NAME)).thenReturn(TEMPLATE_TEXT);
        given(provider.getMessageTemplate(TEMPLATE_NAME)).willReturn(TEMPLATE_TEXT);
        msgBuilder.buildMessage(TEMPLATE_NAME, MESSAGE_TEXT, SIGN);
        inOrder(provider).verify(provider, atLeastOnce()).getMessageTemplate(TEMPLATE_NAME);
    }

    /**
     * Сделать тест, который проверяет, что если шаблон не найден,
     * {@code buildMessage} выбрасывает {@link TemplateException200524}
     */
    @DisplayName("должен кидать нужное исключение, когда зависимости возвращают null вместо шаблона")
    @Test
    public void shouldThrowExceptionWhenDependencyReturnNullInsteadOfTemplate() {
//        Assertions.assertThrows(TemplateException200524.class, () ->
//                new Otus200524MessageBuilderImpl(provider)
//                        .buildMessage(TEMPLATE_NAME, MESSAGE_TEXT, SIGN));
        assertThatThrownBy(() -> new Otus200524MessageBuilderImpl(provider)
                .buildMessage(TEMPLATE_NAME, MESSAGE_TEXT, SIGN))
                .isInstanceOf(TemplateException200524.class);
    }
}
