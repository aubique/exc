package dev.aubique.cw;

import dev.aubique.cw.Cw200530CamelCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Cw200530CamelCaseTest {

    FinalFormat format;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void shouldConvertTextToCamelCase() {
        format = FinalFormat.CAMEL_CASE;
        doAssertions("camel Casing", "camelCasing");
        doAssertions("camel Casing Test", "camelCasingTest");
        doAssertions("camelcasingtest", "camelcasingtest");
    }

    @Test
    void shouldConvertFromCamelCase() {
        format = FinalFormat.NORMAL_TEXT;
        doAssertions("camelCasing", "camel Casing");
        doAssertions("camelCasingTest", "camel Casing Test");
        doAssertions("camelcasingtest", "camelcasingtest");
    }

    private void doAssertions(String testData, String expected) {
        switch (format) {
            case CAMEL_CASE:
                assertEquals(expected, Cw200530CamelCase.toCamelCase(testData));
                break;
            case NORMAL_TEXT:
                assertEquals(expected, Cw200530CamelCase.toNormalText(testData));
                break;
            default:
                throw new RuntimeException("No ENUM data");
        }
    }

    private enum FinalFormat {
        NORMAL_TEXT, CAMEL_CASE;
    }
}
