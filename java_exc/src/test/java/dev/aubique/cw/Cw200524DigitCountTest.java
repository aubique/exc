package dev.aubique.cw;

import dev.aubique.cw.Cw200524DigitCount;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTimeout;

class Cw200524DigitCountTest {

    private static void testing(int actual, int expected) {
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    private static Stream<Arguments> generateDigitCountData() {
        return Stream.of(
                Arguments.of(Cw200524DigitCount.nbDig(5750, 0), 4700),
                Arguments.of(Cw200524DigitCount.nbDig(11011, 2), 9481),
                Arguments.of(Cw200524DigitCount.nbDig(12224, 8), 7733),
                Arguments.of(Cw200524DigitCount.nbDig(11549, 1), 11905)
        );
    }

    @ParameterizedTest
    @MethodSource("generateDigitCountData")
    public void ngDigShouldCalculateValidNumbers(int nbDigResult, int expected) {
        testing(nbDigResult, expected);
    }

    @Test
    public void shouldCalculateValueInTime() {
        assertTimeout(Duration.ofSeconds(1), () -> Cw200524DigitCount.nbDig(42, 4));
    }
}
