package dev.aubique.cw;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Cw200614CarboatTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void BasicTests() {
        assertEquals("[[M: 37 B: 5 C: 4][M: 100 B: 14 C: 11]]",
                Cw200614Carboat.howmuch(1, 100));
        assertEquals("[]",
                Cw200614Carboat.howmuch(2950, 2950));
        assertEquals("[[M: 9991 B: 1427 C: 1110]]",
                Cw200614Carboat.howmuch(10000, 9950));
    }

    @Test
    void showIfInteger() {
        final float x = 100f;
        final float y = (x - 2f) / 7f;
        System.out.println(y);
        System.out.println(y == Math.round(y));
    }

    @Test
    void launchHowMuch() {
        Cw200614Carboat.howmuch(100, 1);
    }

    @Test
    void shouldBeValidForThirtySeven() {
        final String expected = "[M: 37 B: 5 C: 4]";
        assertEquals(expected, Cw200614Carboat.calcCost(37));
    }

    @Test
    void shouldBeValidFor9991() {
        final String expected = "[M: 9991 B: 1427 C: 1110]";
        assertEquals(expected, Cw200614Carboat.calcCost(9991));
    }

    @Test
    public void testCase1() {
        assertEquals(5, Cw200614Carboat.getCount("abracadabra"), "Nope!");
    }

    @Test
    void showChars() {
        final String str = "abcd";
        final char[] chars = str.toCharArray();
        System.out.println(Arrays.toString(chars));
    }

    @Test
    void shouldConfirmVowel() {
        final char vowel = 'a';
        Assertions.assertTrue(Cw200614Carboat.isVowel(vowel));
    }
}
