import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  dateTimeForm!: FormGroup;
  timeCtrl!: FormControl;
  private date = new Date();

  // defaultDateTime = '6/3/2021, 23:26:00';

  constructor() {
    this.dateTimeForm = new FormGroup({
      dateTimeCtrl: new FormControl(this.date, Validators.required),
    });
  }

  ngOnInit(): void {
    this.timeCtrl = new FormControl(this.date, Validators.required);
  }
}
