import { Component } from '@angular/core';
import { PostsService } from '../posts.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
})
export class PostsComponent {

  showIds = false;

  constructor(
    public postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      // console.log('Query Parameters: ', params);
      this.showIds = !!params.showIds;
    });

    this.route.fragment.subscribe((fragment) => {
      // console.log('HTML fragment: ', fragment);
    });
  }

  showIdsProgram() {
    this.router.navigate(['/posts'], {
      queryParams: {
        showIds: true,
      },
      fragment: 'program-fragment',
    });
  }
}
