import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { AboutExtraComponent } from './about-extra/about-extra.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from './auth.guard';
import { PostResolver } from './post.resolver';

// localhost:4200/ -> HomeComponent
// localhost:4200/about -> AboutComponent
// localhost:4200/posts -> PostsComponent
// localhost:4200/posts/:id -> PostComponent
// localhost:4200/about/extra -> AboutExtraComponent
// localhost:4200/error -> ErrorPageComponent
const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'about',
    component: AboutComponent,
    // Authentication for inner page
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'extra', component: AboutExtraComponent,
      }],
  },
  {path: 'posts', component: PostsComponent, canActivate: [AuthGuard]},
  {
    path: 'posts/:id',
    component: PostComponent,
    resolve: {
      post: PostResolver,
    },
  },
  {path: 'error', component: ErrorPageComponent},
  // 404 page should always be the last line
  {path: '**', redirectTo: '/error'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
