import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NewsApiKeyConfig, NgnewsModule } from 'angular-news-api';
import { AppComponent } from './app.component';


const newsApiConfig: NewsApiKeyConfig = {
  key: 'b20d04046fc64438a1e22892479d919e',
};

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    NgnewsModule.forRoot(newsApiConfig),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
