import { Component, OnInit } from '@angular/core';
import { NewsApiService } from 'angular-news-api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'ng-newsapi';

  constructor(private news: NewsApiService) {
  }

  ngOnInit(): void {
    this.news.everything({
      q: 'covid',
      language: 'fr',
      sortBy: 'publishedAt',
    }).subscribe(resp => console.log(resp));
  }
}
