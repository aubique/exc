import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ParentConst } from 'src/app/models/childList';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  $item: Observable<string>;

  ngOnInit(): void {
    const childModel = ParentConst.list[1];
    this.$item = of(childModel.list[1]);
  }
}
