import { ChildModel } from 'src/app/models/child-model';
import { ParentModel } from 'src/app/models/parent-model';

export const ChildList: Array<ChildModel> = [
  {
    name: 'child-1',
    list: ['item-1', 'item-2'],
  }, {
    name: 'child-2',
    list: ['item-3', 'item-4'],
  },
];

export const ParentConst: ParentModel = {
  name: 'parent',
  list: ChildList,
};
