import { ChildModel } from 'src/app/models/child-model';
import { GenericModel } from 'src/app/models/generic-model';

export interface ParentModel extends GenericModel<ChildModel> {
  //name: string;
  //list: Array<ChildModel>;
}
