import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionMaps from 'fusioncharts/fusioncharts.maps';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as World from 'fusioncharts/maps/fusioncharts.world'
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ChartPageComponent } from './chart-page/chart-page.component';
import { WorldPageComponent } from './world-page/world-page.component';

FusionChartsModule.fcRoot(FusionCharts, charts, FusionMaps, World, FusionTheme);

@NgModule({
  declarations: [
    AppComponent,
    ChartPageComponent,
    WorldPageComponent,
  ], imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FusionChartsModule,
  ], providers: [],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {
}
