import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartPageComponent } from 'src/app/chart-page/chart-page.component';
import { WorldPageComponent } from 'src/app/world-page/world-page.component';


const routes: Routes = [
  {path: 'chart', component: ChartPageComponent},
  {path: 'world', component: WorldPageComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ], exports: [RouterModule],
})
export class AppRoutingModule {
}
