import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { VegetableService } from 'src/app/core/models/services/vegetable.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {

  subscriptionOne: Subscription;

  constructor(private service: VegetableService) {
  }

  ngOnInit(): void {
    this.subscriptionOne = this.service.subscribeOnTypeUpdate();
  }

  ngOnDestroy(): void {
    this.subscriptionOne.unsubscribe();
  }
}
