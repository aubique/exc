import { Component, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { TypeVegetableEnum } from 'src/app/core/models/type-vegetable.enum';
import { VegetableService } from 'src/app/core/models/services/vegetable.service';


@Component({
  selector: 'app-inner',
  templateUrl: './inner.component.html',
  styleUrls: ['./inner.component.css'],
})
export class InnerComponent implements OnInit {

  typeState: BehaviorSubject<TypeVegetableEnum>;

  constructor(private service: VegetableService) {
  }

  ngOnInit(): void {
    this.typeState = this.service.typeVegetable$;
  }
}
