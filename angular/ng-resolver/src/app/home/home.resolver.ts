import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { FactoryHelper } from 'src/app/shared/helpers/factory.helper';
import { TypeVegetableEnum } from 'src/app/core/models/type-vegetable.enum';
import { VegetableService } from 'src/app/core/models/services/vegetable.service';


@Injectable({
  providedIn: 'root',
})
export class HomeResolver implements Resolve<null> {

  constructor(
    private service: VegetableService,
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<null> | Promise<null> | null {
    const routeParam = route.params['var'];

    if (this.isParamValid(routeParam)) {
      const typeEnum = this.service.forwardResolve(routeParam);
      this.service.saveLocalStorage(typeEnum);

      return;
    }
    this.service.loadLocalStorage();

    return;
  }

  private isParamValidDepr(param: string): boolean {
    const validParams = [
      TypeVegetableEnum.Tomato.toString(),
      TypeVegetableEnum.Cucumber.toString(),
      TypeVegetableEnum.Potato.toString(),
      TypeVegetableEnum.Carrot.toString(),
    ];
    return validParams.includes(param);
  }

  private isParamValid(param: string): boolean {
    const typeEnum = FactoryHelper.newTypeVegetableEnum(param);
    return typeEnum !== TypeVegetableEnum.Default;
  }
}
