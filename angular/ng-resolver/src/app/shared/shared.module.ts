import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MockComponent } from './components/mock/mock.component';


@NgModule({
  declarations: [MockComponent],
  imports: [
    CommonModule,
  ],
})
export class SharedModule {
}
