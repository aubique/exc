import { TypeVegetableEnum } from 'src/app/core/models/type-vegetable.enum';

export class FactoryHelper {

  static newTypeVegetableEnum(vegetableType: string): TypeVegetableEnum {
    switch (vegetableType) {
      case TypeVegetableEnum.Carrot.toString():
        return TypeVegetableEnum.Carrot;
      case TypeVegetableEnum.Potato.toString():
        return TypeVegetableEnum.Potato;
      case TypeVegetableEnum.Cucumber.toString():
        return TypeVegetableEnum.Cucumber;
      case TypeVegetableEnum.Tomato.toString():
        return TypeVegetableEnum.Tomato;
      default:
        return TypeVegetableEnum.Default;
    }
  }
}
