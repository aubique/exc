import { BehaviorSubject } from 'rxjs';

export interface EnumFactory<T> {

  of(...args: string[]): T;
}
