import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { FactoryHelper } from 'src/app/shared/helpers/factory.helper';
import { TypeVegetableEnum } from 'src/app/core/models/type-vegetable.enum';

@Injectable({
  providedIn: 'root',
})
export class VegetableService {

  static readonly STORAGE_ITEM_NAME = 'vegetable-type';
  public typeVegetable$ = new BehaviorSubject<TypeVegetableEnum>(
    FactoryHelper.newTypeVegetableEnum(
      TypeVegetableEnum.Tomato));

  constructor(
    private router: Router,
  ) {
  }

  public navigateTo(typeEnum: TypeVegetableEnum): void {
    let url = this.typeVegetable$.getValue().toString();

    if (typeEnum !== null)
      url = typeEnum.toString();

    this.router.navigate([url]);
  }

  public loadLocalStorage(): void {
    const vegetableTxt = localStorage.getItem(VegetableService.STORAGE_ITEM_NAME);
    const typeEnum = FactoryHelper.newTypeVegetableEnum(vegetableTxt);

    if (typeEnum !== TypeVegetableEnum.Default) {
      this.typeVegetable$ = new BehaviorSubject<TypeVegetableEnum>(typeEnum);
    }

    const type = this.typeVegetable$.getValue();
    this.navigateTo(type);
  }

  public onEvent(typeEnum: TypeVegetableEnum): void {
    this.navigateTo(typeEnum);
  }

  public saveLocalStorage(typeEnum: TypeVegetableEnum): void {
    localStorage.setItem(VegetableService.STORAGE_ITEM_NAME, typeEnum.toString());
  }

  public forwardResolve(typeVeganTxt: string): TypeVegetableEnum {
    const typeEnum = FactoryHelper.newTypeVegetableEnum(typeVeganTxt);

    this.typeVegetable$.next(typeEnum);
    return typeEnum;
  }

  public subscribeOnTypeUpdate(): Subscription {
    return this.typeVegetable$.pipe(
      filter(t => t !== undefined && t !== null),
    ).subscribe(veganType => {
      console.log('onTypeUpdate:', veganType);
    });
  }
}
