import { Injectable } from '@angular/core';
import { EnumFactory } from 'src/app/core/models/enum-factory';
import { TypeVegetableEnum } from 'src/app/core/models/type-vegetable.enum';

@Injectable({
  providedIn: 'root',
})
export class FactoryService {
}

export namespace FactoryService {

  export class TypeVegetable implements EnumFactory<TypeVegetableEnum> {

    constructor() {
    }

    of(vegetableType: string): TypeVegetableEnum {
      switch (vegetableType) {
        case TypeVegetableEnum.Carrot.toString():
          return TypeVegetableEnum.Carrot;
        case TypeVegetableEnum.Potato.toString():
          return TypeVegetableEnum.Potato;
        default:
          return TypeVegetableEnum.Carrot;
      }
    }
  }
}
