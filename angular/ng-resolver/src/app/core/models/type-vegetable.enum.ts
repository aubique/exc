export enum TypeVegetableEnum {
  Default = 'default',
  Potato = 'potato',
  Carrot = 'carrot',
  Cucumber = 'cucumber',
  Tomato = 'tomato',
}
