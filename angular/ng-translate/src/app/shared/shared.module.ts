import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LocaleComponent } from 'src/app/shared/locale/locale.component';

@NgModule({
  declarations: [LocaleComponent],
  imports: [
    CommonModule,
    TranslateModule,
    NgbModule,
    FontAwesomeModule,
  ],
  exports: [
    TranslateModule,
    NgbModule,
    FontAwesomeModule,

    LocaleComponent,
  ],
})
export class SharedModule {
}
