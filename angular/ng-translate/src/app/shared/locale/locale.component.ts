import { Component, OnDestroy, OnInit } from '@angular/core';

import { faLanguage } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { StoreService } from 'src/app/services/store.service';


@Component({
  selector: 'app-locale',
  templateUrl: './locale.component.html',
  styleUrls: ['./locale.component.css'],
})
export class LocaleComponent implements OnInit, OnDestroy {

  enlang: string;
  nllang: string;
  private subEnlang: Subscription;
  private subNllang: Subscription;
  faLanguage = faLanguage;

  constructor(
    public translate: TranslateService,
    public store: StoreService,
  ) {
  }

  ngOnInit(): void {
    this.initTranslateSubscription();
  }

  ngOnDestroy(): void {
    this.subEnlang.unsubscribe();
    this.subNllang.unsubscribe();
  }

  switchLang(lang: string): void {
    this.translate.use(lang);
  }

  private initTranslateSubscription(): void {
    this.subEnlang = this.translate.stream('Language.Enlang').subscribe(res => {
      this.enlang = res;
      console.log('Enlang:', res);
    });
    this.subNllang = this.translate.stream('Language.Nllang').subscribe(res => {
      this.nllang = res;
      console.log('Nllang:', res);
    });
  }
}
