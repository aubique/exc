import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreService {

  public languagePack = [
    {
      name: 'Enlang',
      lang: 'en',
    },
    {
      name: 'Nllang',
      lang: 'nl',
    },
  ];


  constructor() {
  }
}
