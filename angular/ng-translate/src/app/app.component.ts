import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { StoreService } from 'src/app/services/store.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  constructor(
    public translate: TranslateService,
    private store: StoreService,
  ) {
  }

  ngOnInit(): void {
    let browserLang = this.translate.getBrowserLang();
    if (this.store.languagePack.map(l => l.lang).indexOf(browserLang) > -1) {
      console.log('Set language of the browser');

      this.translate.setDefaultLang(browserLang);
    } else {
      console.log('Set the default locale, EN');

      this.translate.setDefaultLang('en');
    }

    console.log(this.store.languagePack.map(l => l.lang).indexOf(browserLang));
  }
}
