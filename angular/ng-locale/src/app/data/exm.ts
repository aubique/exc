import '@angular/localize/init';

export const Exm = {
  yesterday: $localize`25 June Tuesday`,
  today: $localize`26 June Friday`,
  tomorrow: $localize`27 June Saturday`,
};
