import { Component, Inject, LOCALE_ID } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  title = 'ng-locale';

  languageList = [
    {code: 'en', label: 'English'},
    {code: 'hi', label: 'हिंदी'},
    {code: 'es', label: 'Espanol'},
  ];

  constructor(@Inject(LOCALE_ID) protected localeId: string) {
  }
}
