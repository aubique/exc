export enum SeasonEnum {
  Winter = 'WINTER',
  Spring = 'SPRING',
  Summer = 'SUMMER',
  Autumn = 'AUTUMN',
}
