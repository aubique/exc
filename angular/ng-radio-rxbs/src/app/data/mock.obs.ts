import { from, Observable, of } from 'rxjs';
import { concatMap, delay } from 'rxjs/operators';
import { MockModel } from 'src/app/data/mock.model';

export const MockObs = from([
  {w: 'first winter day', d: 'first day of another season'},
  {w: '2nd winter day', d: '2nd day of another season'},
  {w: '3rd winter day', d: '3rd day of another season'},
  {w: '4 winter day', d: '4 day of another season'},
  {w: '5 winter day', d: '5 day of another season'},
  {w: '6 winter day', d: '6 day of another season'},
  {w: '7 winter day', d: '7 day of another season'},
  {w: '8 winter day', d: '8 day of another season'},
  {w: '9 winter day', d: '9 day of another season'},
  {w: '10 winter day', d: '10 day of another season'},
  {w: '11 winter day', d: '11 day of another season'},
  {w: '12 winter day', d: '12 day of another season'},
  {w: '13 winter day', d: '13 day of another season'},
  {w: '14 winter day', d: '14 day of another season'},
  {w: '15 winter day', d: '15 day of another season'},
  {w: '16 winter day', d: '16 day of another season'},
  {w: '17 winter day', d: '17 day of another season'},
  {w: '18 winter day', d: '18 day of another season'},
  {w: '19 winter day', d: '19 day of another season'},
  {w: '20 winter day', d: '20 day of another season'},
]).pipe(
  concatMap(obj => of(obj).pipe(delay(5000))),
) as Observable<MockModel>;
