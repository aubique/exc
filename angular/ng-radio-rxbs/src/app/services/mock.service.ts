import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { MockModel } from 'src/app/data/mock.model';
import { MockObs } from 'src/app/data/mock.obs';
import { SeasonEnum } from 'src/app/data/season.enum';
import { StoreService } from 'src/app/services/store.service';

@Injectable({
  providedIn: 'root',
})
export class MockService {

  constructor(private store: StoreService) {
  }

  public triggerByMockObs(): Subscription {
    return MockObs.subscribe(obj => {
      const lastSeason = this.store.seasonState$.getValue();
      console.log('MOCK has been changed');
      console.log(obj);

      this.pushNextPhrase(lastSeason, obj);
    });
  }

  public triggerBySeasonObs(): Subscription {
    return this.store.seasonState$.subscribe((season) => {
      const lastMock = this.store.mockState$.getValue();
      console.log('SEASON has been changed');

      this.pushNextPhrase(season, lastMock);
    });
  }

  private pushNextPhrase(season: SeasonEnum, mockObj: MockModel): void {
    const mockFieldDetermined = MockService.determineSeasonType(season, mockObj);
    return this.store.phraseState$.next(mockFieldDetermined);
  }

  private static determineSeasonType(season: SeasonEnum, mockObj: MockModel): string {
    if (season == SeasonEnum.Winter)
      return mockObj.w;
    return mockObj.d;
  }

  public initMockSubject(): Subscription {
    return MockObs.subscribe(obj => this.store.mockState$.next(obj));
  }
}
