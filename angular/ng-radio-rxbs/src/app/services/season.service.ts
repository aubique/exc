import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SeasonEnum } from 'src/app/data/season.enum';
import { StoreService } from 'src/app/services/store.service';

@Injectable({
  providedIn: 'root',
})
export class SeasonService {

  private static readonly STORAGE_SEASON = 'season';

  constructor(private store: StoreService) {
  }

  get seasonState$(): BehaviorSubject<SeasonEnum> {
    return this.store.seasonState$;
  }

  get phraseState$(): BehaviorSubject<string> {
    return this.store.phraseState$;
  }

  public loadSeason(): void {
    let storageSeason: string;

    storageSeason = localStorage.getItem(SeasonService.STORAGE_SEASON);
    this.store.seasonState$ = SeasonService.newSeasonSubject(storageSeason);
  }

  private static newSeasonSubject(storageSeason: string): BehaviorSubject<SeasonEnum> {
    switch (storageSeason) {
      case SeasonEnum.Winter.toString():
        return new BehaviorSubject<SeasonEnum>(SeasonEnum.Winter);
      case SeasonEnum.Spring.toString():
        return new BehaviorSubject<SeasonEnum>(SeasonEnum.Spring);
      case SeasonEnum.Summer.toString():
        return new BehaviorSubject<SeasonEnum>(SeasonEnum.Summer);
      case SeasonEnum.Autumn.toString():
        return new BehaviorSubject<SeasonEnum>(SeasonEnum.Autumn);
      default:
        return new BehaviorSubject<SeasonEnum>(SeasonEnum.Winter);
    }
  }

  public saveSeason(seasonValue: SeasonEnum): void {
    this.store.seasonState$.next(seasonValue);
    localStorage.setItem(SeasonService.STORAGE_SEASON, seasonValue.toString());
  }
}
