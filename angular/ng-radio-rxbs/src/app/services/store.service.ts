import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MockModel } from 'src/app/data/mock.model';
import { SeasonEnum } from 'src/app/data/season.enum';

@Injectable({
  providedIn: 'root',
})
export class StoreService {

  public seasonState$: BehaviorSubject<SeasonEnum>;
  public phraseState$ = new BehaviorSubject<string>('nothing');
  public mockState$ = new BehaviorSubject<MockModel>({w: 'nonew', d: 'noned'} as MockModel);
}
