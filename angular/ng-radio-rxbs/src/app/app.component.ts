import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SeasonEnum } from 'src/app/data/season.enum';
import { MockService } from 'src/app/services/mock.service';
import { SeasonService } from 'src/app/services/season.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {

  seasonState: BehaviorSubject<SeasonEnum>;
  phraseState: BehaviorSubject<string>;

  springEnum = SeasonEnum.Spring;
  winterEnum = SeasonEnum.Winter;
  summerEnum = SeasonEnum.Summer;
  autumnEnum = SeasonEnum.Autumn;

  mockSub: Subscription;
  obsToBehSub: Subscription;
  seasonSub: Subscription;

  constructor(private season: SeasonService, private mock: MockService) {
  }

  ngOnInit(): void {
    this.season.loadSeason();
    this.seasonState = this.season.seasonState$;
    this.doSubscriptions();
    this.phraseState = this.season.phraseState$;
  }

  private doSubscriptions(): void {
    this.obsToBehSub = this.mock.initMockSubject();
    this.mockSub = this.mock.triggerByMockObs();
    this.seasonSub = this.mock.triggerBySeasonObs();
  }

  ngOnDestroy(): void {
    this.obsToBehSub.unsubscribe();
    this.mockSub.unsubscribe();
    this.seasonSub.unsubscribe();
  }

  onSelectionChange(event): void {
  }

  onClick(seasonEnum: SeasonEnum): void {
    this.season.saveSeason(seasonEnum);
  }
}
