import {Injectable} from '@angular/core';
import {Init} from '../init-markers';

@Injectable()
export class MarkerService extends Init {

  constructor() {
    super();
    console.log('MarkerService initialized...');
    this.load();
  }

  getMarkers() {
    return JSON.parse(localStorage.getItem('markers'));
  }

  addMarker(newMarker: { lng: number; draggable: boolean; name: string; lat: number }) {
    // Fetch markers from localStorage
    const markers = JSON.parse(localStorage.getItem('markers'));
    // Push to array
    markers.push(newMarker);
    // Set ls markers again
    localStorage.setItem('markers', JSON.stringify(markers));
  }

  updateMarker(marker, newLat, newLng) {
    // Fetch markers from localStorage
    const lsMarkerList = JSON.parse(localStorage.getItem('markers'));

    for (const lsMarker of lsMarkerList) {
      if (marker.lat === lsMarker.lat && marker.lng === lsMarker.lng) {
        lsMarker.lat = newLat;
        lsMarker.lng = newLng;
      }
    }

    // Set ls markers again
    localStorage.setItem('markers', JSON.stringify(lsMarkerList));
  }

  removeMarker(marker) {
    // Fetch markers from localStorage
    const lsMarkerList = JSON.parse(localStorage.getItem('markers'));

    // Find and splice out of list
    for (let i; i < lsMarkerList.length; i++) {
      if (marker.lat === lsMarkerList[i].lat && marker.lng === lsMarkerList[i].lng) {
        lsMarkerList.splice(i, 1);
      }
    }

    // Set ls markers again
    localStorage.setItem('markers', JSON.stringify(lsMarkerList));
  }
}
