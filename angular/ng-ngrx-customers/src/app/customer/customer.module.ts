import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomViewComponent } from './custom-view/custom-view.component';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { StoreModule } from '@ngrx/store';
import { customerFeatureKey, reducer } from './reducer/customer.reducer';


@NgModule({
  declarations: [
    CustomViewComponent,
    CustomerAddComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(customerFeatureKey, reducer),
  ],
  exports: [
    CustomViewComponent,
    CustomerAddComponent,
  ],
})
export class CustomerModule {
}
