import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { CustomerState } from '../reducer/customer.reducer';
import { selectCustomers } from '../store/selector/customer.selectors';
import { Observable } from 'rxjs';
import { Customer } from '../../models/customer';

@Component({
  selector: 'app-custom-view',
  templateUrl: './custom-view.component.html',
  styleUrls: ['./custom-view.component.css'],
})
export class CustomViewComponent implements OnInit {

  customers$: Observable<Array<Customer>>;

  constructor(private store: Store<CustomerState>) {
    this.customers$ = this.store.pipe(select(selectCustomers));
  }

  ngOnInit(): void {
  }
}
